"""
Question 3:
	- Synchronizing the threads with lock
"""
import threading
import logging

from cars import Car

lock = threading.Lock()
#lock = threading.RLock() #using RLock

def run(car):
	""" Run the car"""	
	lock.acquire()
	car.run()
	lock.release()

if __name__ == "__main__":
	format = "%(asctime)s: %(message)s"
	logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
	logging.info("Create two cars")
	toyota = Car(4,4,5,100)
	bmw = Car(4,4,5,200)

	logging.info("Start the race with lock")
	threads = []	
	# Create two threads
	t1 = threading.Thread(target = run, args=(toyota,))
	t2 = threading.Thread(target = run, args=(bmw,))
	
	#Start the threads
	t1.start()
	t2.start()
	
	# Add the two threads to a threads list
	threads.append(t1)
	threads.append(t2)
	
	# Complete each threads
	for t in threads:
		t.join()
	
	logging.info("Complete the biased race")
