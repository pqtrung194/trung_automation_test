"""
Question 4:
	- Run the cars side by side using Queu and lock
"""
from queue import Queue
import logging
import threading

from cars import Car

lock = threading.Lock()
#lock = threading.RLock() #using RLock

exit_flag = False
#Initiize a que
q = Queue(maxsize=1) 

def run(car):
	""" Run the car"""	
	while not exit_flag:
		lock.acquire()
		if not q.empty():
			logging.info('Start car')
			car.run()			
			q.task_done()
			lock.release()
		else:
			lock.release()

if __name__ == "__main__":
	format = "%(asctime)s: %(message)s"
	logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
	logging.info("Create two cars")
	toyota = Car(4,4,5,100)
	bmw = Car(4,4,5,200)

	logging.info("Start the race")
	threads = []	
	# Create two threads
	t1 = threading.Thread(target = run, args=(toyota,))
	t2 = threading.Thread(target = run, args=(bmw,))
	
	#Start the threads
	t1.start()
	t2.start()
	
	# Add the two threads to a threads list
	threads.append(t1)
	threads.append(t2)
	# Fill the queue
	
	for thread in threads:
		logging.info('it works up to this point')
		q.put(thread)
	lock.realse()
	#logging.info('it works up to this point')
	
	# Check that the queue is empty
	while not q.empty():
		pass
		
	exit_flag = True
	# Complete each threads
	for t in threads:
		t.join()
	
	logging.info("Complete the race")
