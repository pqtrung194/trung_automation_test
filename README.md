# Trung_automation_test

## Question 1:

 - Run the following command to create the cars and run them:
 	
	```
 	$ python create_cars_and_run.py
	```
	
 - Sample output:
 
```bash
 	9:33:31: Create a Toyota and run it	
	100	
	100	
	100	
	100	
	100	
	100	
	100	
	100	
	100	
	100	
	9:30:33: Create a BMW and run it
	200	
	200	
	200	
	200	
	200	
	200	
	200	
	200	
	200	
	200
```

## Question 2: 

   Run the following command to create the cars and run them in threads:

	```bash
	$ python run_cars_in_threads.py
	```
  The output are different from time to time. Below is a sample output:
	
```bash
	10:14:00: Create two cars
	10:14:00: Start the race
	100
	100
	200
	100
	200
	100
	200
	100
	200
	100
	200
	100
	200
	100
	200
	100
	200
	100
	200
	200
```

## Question 3: 

	The results in question 2 and question 3 are different beacause in question 2, the two tasks are running simultaneously at the same time. In order to make sure that the output is still the same, we can implement some synchronization machenisms, such as Lock, Rlock, Barriers, Semaphores. In this example, I used the Lock
	
Run the following command to create the cars and run them in threads:

```bash
$ python sync_run_cars_in_threads.py
```

The output now is as follows:

```bash
	11:31:23: Create two cars
	11:31:23: Start the race with lock
	100
	100
	100
	100
	100
	100
	100
	100
	100
	100
	200
	200
	200
	200
	200
	200
	200
	200
	200
	200
	11:31:23: Complete the biased race
```

## Question 4:

In order to get the results as in the question, the idea would be put all the threads in to a queue that can hold 1 item at a time, together with multi-threadings and a lock mechanism to archieve the desired result. 

Run the below command to check the result (still debugging!)

```bash
$ python run_cars_side_by_side.py
```
	
	
