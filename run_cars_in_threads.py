"""
Question 2:
	- Create the two cars
	- Run the two cars in threads
"""
import threading
import logging

from cars import Car
from create_cars_and_run import run


if __name__ == "__main__":
	format = "%(asctime)s: %(message)s"
	logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
	logging.info("Create two cars")
	toyota = Car(4,4,5,100)
	bmw = Car(4,4,5,200)

	logging.info("Start the race")
	t1 = threading.Thread(target = run, args=(toyota,))
	t2 = threading.Thread(target = run, args=(bmw,))
	t1.start()
	t2.start()
	#logging.info("Joining steps")
	t1.join()
	t2.join()
	
