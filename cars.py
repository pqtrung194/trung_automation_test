# Model for Question 1


class Car:
	#wheels = 4
	def __init__(self, wheels, doors, seats, maxSpeed):
		self.wheels = wheels
		self.doors = doors
		self.seats = seats
		self.maxSpeed = maxSpeed
	
	def run(self):
		"""print out the max speed of the car in a loop of 10 times"""
		for i in range(10):
			print(self.maxSpeed)

	def info(self):
		"""Get all the characteristics of the car"""
		print('Characteristic of the car: ')
		print('wheels = {0}\ndoors = {1}\nseats = {2}\nmaxSpeed = {3}\n'.format(
				self.wheels, self.doors, self.seats, self.maxSpeed ))
		
