"""
	Question 1.
	This script creats two cars and apply the run() method on them
"""
import logging
from cars import Car

def run(car):
	""" Run the car"""	
	car.run()

if __name__ == "__main__":
	format = "%(asctime)s: %(message)s"
	logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
	logging.info("Create a Toyata and run it")
	toyota = Car(4,4,5,100)
	run(toyota)
	
	logging.info("Create a BMW and run it")
	bmw = Car(4,4,5,200)
	run(bmw)
